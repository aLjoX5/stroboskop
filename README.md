# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://aLjoX5@bitbucket.org/aLjoX5/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/aLjoX5/stroboskop/commits/47fd44d32a3fba01d168016ec2039edd3acc7eac

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/aLjoX5/stroboskop/commits/934d80185d944051582bd2b3d408f8e04b8fd9cc

Naloga 6.3.2:
https://bitbucket.org/aLjoX5/stroboskop/commits/4a8f46d6ef269120df781f649b591f78a69ea1aa

Naloga 6.3.3:
https://bitbucket.org/aLjoX5/stroboskop/commits/41a60402beba708fd46ab59a9e27dedc7445cf4d

Naloga 6.3.4:
https://bitbucket.org/aLjoX5/stroboskop/commits/135cf2e6097313d83fbd2a2ca629960be708cbb8

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/aLjoX5/stroboskop/commits/d8051914382058b336b841829d88e3296e80167e

Naloga 6.4.2:
https://bitbucket.org/aLjoX5/stroboskop/commits/519c61e2f0932f450f490bbb528f98b099eb085e

Naloga 6.4.3:
https://bitbucket.org/aLjoX5/stroboskop/commits/8a94861213b2df88ace98aeb625fec1f09a03921

Naloga 6.4.4:
https://bitbucket.org/aLjoX5/stroboskop/commits/f28420e2fa7d2a1a2387c93050af4af56e56c414